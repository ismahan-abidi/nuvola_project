#!/bin/bash

# création d'un VPC, d'un sous réseau, d'une passerelle et d'une table de routage pour avoir seulement nos machines dans le même réseau
echo "Création d'un VPC pour les deux machines"
test -f /etc/aws_puppet/vpc
checkvpc=$?
if [ $checkvpc == 1 ] ; then
    # création d'un VPC
    aws ec2 create-vpc --cidr-block 172.40.0.0/16 --query Vpc.VpcId --output text > /etc/aws_puppet/vpc
    idVPC=$(cat /etc/aws_puppet/vpc)
    echo "VPC : "$idVPC
    aws ec2 modify-vpc-attribute --vpc-id $idVPC --enable-dns-support
    aws ec2 modify-vpc-attribute --vpc-id $idVPC --enable-dns-hostnames
    aws ec2 create-tags --resources $idVPC --tags Key=Name,Value=VPC_NUVOLA

    # création du sous réseau
    aws ec2 create-subnet --vpc-id $idVPC --cidr-block 172.40.16.0/24 --query Subnet.SubnetId --output text > /etc/aws_puppet/subnet
    aws ec2 create-subnet --vpc-id $idVPC --cidr-block 172.40.0.0/24 --query Subnet.SubnetId --output text > /etc/aws_puppet/subnetDefault
    idsubnet=$(cat /etc/aws_puppet/subnet)
    idsubnetDefault=$(cat /etc/aws_puppet/subnetDefault)
    echo "Sous-réseau :" $idsubnet
    echo "Sous-réseau par défaut :" $idsubnetDefault
    aws ec2 create-tags --resources $idsubnet --tags Key=Name,Value=SubnetDefault_NUVOLA
    aws ec2 create-tags --resources $idsubnetDefault --tags Key=Name,Value=Subnet_NUVOLA

    # création de la passerelle
    aws ec2 create-internet-gateway --query InternetGateway.InternetGatewayId --output text > /etc/aws_puppet/gateway
    idGateway=$(cat /etc/aws_puppet/gateway)
    echo "Passerelle :" $idGateway
    aws ec2 create-tags --resources $idGateway --tags Key=Name,Value=Gateway_NUVOLA

    # on attache notre passerelle au VPC
    aws ec2 attach-internet-gateway --vpc-id $idVPC --internet-gateway-id $idGateway

    # on cré la table de routage à notre VPC
    aws ec2 create-route-table --vpc-id $idVPC --query RouteTable.RouteTableId --output text > /etc/aws_puppet/routeTable
    idRouteTable=$(cat /etc/aws_puppet/routeTable)
    echo "Table de routage :" $idRouteTable
    aws ec2 create-tags --resources $idRouteTable --tags Key=Name,Value=RouteTable_NUVOLA

    # acheminement table routage vers passerelle
    aws ec2 create-route --route-table-id $idRouteTable --destination-cidr-block 0.0.0.0/0 --gateway-id $idGateway

    # vérification de cet acheminement
    aws ec2 describe-route-tables --route-table-id $idRouteTable
    aws ec2 describe-subnets --filters "Name=vpc-id,Values=$idVPC" --query "Subnets[*].{ID:SubnetId,CIDR:CidrBlock}" > /etc/aws_puppet/subnetID
    # obtention de l'id de notre sous réseau
    ssreseau=$(cat /etc/aws_puppet/subnetID | awk '/"ID":/' | cut -d ":" -f2 | tr -d '"' | tr -d ',' | tr -d ' ' | sed -n '2 p')
    aws ec2 associate-route-table --subnet-id $ssreseau --route-table-id $idRouteTable
    aws ec2 modify-subnet-attribute --subnet-id $ssreseau --map-public-ip-on-launch
    # vérification de la création de notre VPC
    aws ec2 describe-route-tables --route-table-id $idRouteTable
    aws ec2 associate-route-table --subnet-id $idsubnetDefault --route-table-id $idRouteTable
fi

# création de la clé (admin) si elle n'existe pas encore
echo "Création d'une paire de clé pour l'administrateur"
test -f /etc/aws_puppet/masterKeyInstance.pem
check=$?
if [ $check == 1 ] ; then
    mkdir -p /etc/aws_puppet
    aws ec2 create-key-pair --key-name masterKeyInstance --query 'KeyMaterial' --output text > /etc/aws_puppet/masterKeyInstance.pem
    chmod 600 /etc/aws_puppet/masterKeyInstance.pem
fi
# création du groupe de sécurité
echo "Création d'un groupe de sécurité"
idVPC=$(cat /etc/aws_puppet/vpc)
test -f /etc/aws_puppet/sg_NUVOLA
checkSg=$?
if [ $checkSg == 1 ] ; then
    aws ec2 create-security-group --group-name Security_Group_NUVOLA --description "SG of NUVOLA" --vpc-id $idVPC > /etc/aws_puppet/sg_NUVOLA
    idSg=$(cat /etc/aws_puppet/sg_NUVOLA | grep GroupId | cut -d \" -f4)
    aws ec2 create-tags --resources $idSg --tags Key=Name,Value=NUVOLA_Security_Group
    aws ec2 authorize-security-group-ingress --group-id $idSg --protocol tcp --port 8140 --cidr 0.0.0.0/0
    aws ec2 authorize-security-group-ingress --group-id $idSg --protocol tcp --port 22 --cidr 0.0.0.0/0
fi

# création de ec2 master (admin)
echo "Création d'une instance administrateur EC2"
ssreseau=$(cat /etc/aws_puppet/subnetID | awk '/"ID":/' | cut -d ":" -f2 | tr -d '"' | tr -d ',' | tr -d ' ' | sed -n '2 p')
test -f /etc/aws_puppet/master_instance
checkInstance=$?
if [ $checkInstance == 1 ] ; then
    aws ec2 run-instances --image-id ami-03f6d497fceb40069 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Master_NUVOLA}]' --count 1 --instance-type t2.medium --key-name masterKeyInstance --security-group-ids $idSg --subnet-id $ssreseau > /etc/aws_puppet/master_instance
    echo "En attente du démarrage de la machine à distance... Cela va prendre environ 2 minutes."
    sleep 30
    echo "---------                 (33%)"
    sleep 30
    echo "----------------          (66%)"
    sleep 30
    echo "------------------------- (100%)"
    echo "Instance lancée"
fi
# pour récupérer l'adresse ip publique de notre instance et l'ajouter dans le fichier "hosts" :
id_master_inst=$(cat /etc/aws_puppet/master_instance | grep -i InstanceId | cut -d \" -f4)
aws ec2 describe-instances --instance-ids $id_master_inst --query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text | cut -d' ' -f1 > /etc/aws_puppet/ipPublicMaster
echo "L'adresse ip publique de l'instance EC2 administrateur est :" $(cat /etc/aws_puppet/ipPublicMaster)

# connexion ssh pour l'instance ec2 master
echo "Tentative de connexion à l'instance EC2 administrateur via ssh..."
ipV4_master_instance=$(cat /etc/aws_puppet/ipPublicMaster)
ssh -tt -i /etc/aws_puppet/masterKeyInstance.pem -o StrictHostKeyChecking=no ubuntu@$ipV4_master_instance "sudo git clone https://gitlab.com/ismahan-abidi/nuvola_project.git -b installPuppet; cd nuvola_project; sudo chmod 600 puppetMaster.sh; sudo bash puppetMaster.sh; exit"