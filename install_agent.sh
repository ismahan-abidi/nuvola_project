#!/bin/bash

# création de la clé (développeur) si elle n'existe pas encore
echo "Création d'une paire de clé pour l'agent (développeur)"
test -f /etc/aws_puppet/agentKeyInstance.pem
check=$?
if [ $check == 1 ] ; then
    mkdir -p /etc/aws_puppet
    aws ec2 create-key-pair --key-name agentKeyInstance --query 'KeyMaterial' --output text > /etc/aws_puppet/agentKeyInstance.pem
    chmod 600 /etc/aws_puppet/agentKeyInstance.pem
fi

# création de l'agent (machin de développeur)
echo "Création d'une instance développeur EC2"
ssreseau=$(cat /etc/aws_puppet/subnetID | awk '/"ID":/' | cut -d ":" -f2 | tr -d '"' | tr -d ',' | tr -d ' ' | sed -n '2 p')
test -f /etc/aws_puppet/agent_instance
checkInstance=$?
if [ $checkInstance == 1 ] ; then
    idSg=$(cat /etc/aws_puppet/sg_NUVOLA | grep GroupId | cut -d \" -f4)
    ssreseau=$(cat /etc/aws_puppet/subnetID | awk '/"ID":/' | cut -d ":" -f2 | tr -d '"' | tr -d ',' | tr -d ' ' | sed -n '2 p')
    aws ec2 run-instances --image-id ami-03f6d497fceb40069 --tag-specifications 'ResourceType=instance,Tags=[{Key=Name,Value=Agent_NUVOLA}]' --count 1 --instance-type t2.medium --key-name agentKeyInstance --security-group-ids $idSg --subnet-id $ssreseau > /etc/aws_puppet/agent_instance
    echo "En attente du démarrage de la machine à distance... Cela va prendre environ 2 minutes."
    sleep 30
    echo "---------                 (33%)"
    sleep 30
    echo "----------------          (66%)"
    sleep 30
    echo "------------------------- (100%)"
    echo "Instance lancée"
fi
# pour récupérer l'adresse ip publique de notre instance et l'ajouter dans le fichier "hosts" :
id_agent_inst=$(cat /etc/aws_puppet/agent_instance | grep -i InstanceId | cut -d \" -f4)
aws ec2 describe-instances --instance-ids $id_agent_inst --query 'Reservations[*].Instances[*].[PublicIpAddress]' --output text | cut -d' ' -f1 > /etc/aws_puppet/ipPublicAgent
echo "L'adresse ip publique de la machine AGENT est :" $(cat /etc/aws_puppet/ipPublicAgent)

# connexion ssh pour l'instance ec2 agent
echo "Tentative de connexion à l'instance EC2 via ssh..."
ipV4_agent_instance=$(cat /etc/aws_puppet/ipPublicAgent)
ipV4_master_instance=$(cat /etc/aws_puppet/ipPublicMaster)
ssh -tt -i /etc/aws_puppet/agentKeyInstance.pem -o StrictHostKeyChecking=no ubuntu@$ipV4_agent_instance "sudo echo '$ipV4_master_instance' 'puppet' >> /home/ubuntu/ipMaster; sudo git clone https://gitlab.com/ismahan-abidi/nuvola_project.git -b installPuppet; cd nuvola_project; sudo chmod 600 puppetAgent.sh; sudo bash puppetAgent.sh; exit"